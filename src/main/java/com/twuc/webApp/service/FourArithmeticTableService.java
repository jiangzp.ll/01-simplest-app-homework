package com.twuc.webApp.service;

import org.springframework.stereotype.Service;

@Service
public class FourArithmeticTableService {

    public String getPlusTable(Integer start, Integer end) {

        StringBuilder plusTable = new StringBuilder();

        for (int i = start; i <= end; i++) {
            for (int j = start; j <= end; j++) {
                if (i + j <10) {
                    plusTable.append(String.format("%d+%d=%d  ", i, j, i + j));
                } else {
                    plusTable.append(String.format("%d+%d=%d ", i, j, i + j));
                }
            }
            plusTable.append("\n");
        }
        return plusTable.toString();
    }

    public String getMultiplicationTable(Integer start, Integer end) {
        StringBuilder multiplicationTable = new StringBuilder();

        for (int i = start; i <= end; i++) {
            for (int j = start; j <= end; j++) {
                if (i * j <10) {
                    multiplicationTable.append(String.format("%dx%d=%d  ", i, j, i * j));
                } else {
                    multiplicationTable.append(String.format("%dx%d=%d ", i, j, i * j));
                }
            }
            multiplicationTable.append("\n");
        }
        return multiplicationTable.toString();
    }
}
