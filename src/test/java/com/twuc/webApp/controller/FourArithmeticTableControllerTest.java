package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
class FourArithmeticTableControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_get_plus_tables_success() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string(
                        "1+1=2  1+2=3  1+3=4  1+4=5  1+5=6  1+6=7  1+7=8  1+8=9  1+9=10 \n" +
                                "2+1=3  2+2=4  2+3=5  2+4=6  2+5=7  2+6=8  2+7=9  2+8=10 2+9=11 \n" +
                                "3+1=4  3+2=5  3+3=6  3+4=7  3+5=8  3+6=9  3+7=10 3+8=11 3+9=12 \n" +
                                "4+1=5  4+2=6  4+3=7  4+4=8  4+5=9  4+6=10 4+7=11 4+8=12 4+9=13 \n" +
                                "5+1=6  5+2=7  5+3=8  5+4=9  5+5=10 5+6=11 5+7=12 5+8=13 5+9=14 \n" +
                                "6+1=7  6+2=8  6+3=9  6+4=10 6+5=11 6+6=12 6+7=13 6+8=14 6+9=15 \n" +
                                "7+1=8  7+2=9  7+3=10 7+4=11 7+5=12 7+6=13 7+7=14 7+8=15 7+9=16 \n" +
                                "8+1=9  8+2=10 8+3=11 8+4=12 8+5=13 8+6=14 8+7=15 8+8=16 8+9=17 \n" +
                                "9+1=10 9+2=11 9+3=12 9+4=13 9+5=14 9+6=15 9+7=16 9+8=17 9+9=18 \n"));
    }

    @Test
    void should_get_3_to_6_puls_table() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus")
                .param("start","3").param("end", "6"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string(
                        "3+3=6  3+4=7  3+5=8  3+6=9  \n" +
                                "4+3=7  4+4=8  4+5=9  4+6=10 \n" +
                                "5+3=8  5+4=9  5+5=10 5+6=11 \n" +
                                "6+3=9  6+4=10 6+5=11 6+6=12 \n"));
    }

    @Test
    void should_return_200_when_get_multiplication_tables_success() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiplication"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string(
                        "1x1=1  1x2=2  1x3=3  1x4=4  1x5=5  1x6=6  1x7=7  1x8=8  1x9=9  \n" +
                                "2x1=2  2x2=4  2x3=6  2x4=8  2x5=10 2x6=12 2x7=14 2x8=16 2x9=18 \n" +
                                "3x1=3  3x2=6  3x3=9  3x4=12 3x5=15 3x6=18 3x7=21 3x8=24 3x9=27 \n" +
                                "4x1=4  4x2=8  4x3=12 4x4=16 4x5=20 4x6=24 4x7=28 4x8=32 4x9=36 \n" +
                                "5x1=5  5x2=10 5x3=15 5x4=20 5x5=25 5x6=30 5x7=35 5x8=40 5x9=45 \n" +
                                "6x1=6  6x2=12 6x3=18 6x4=24 6x5=30 6x6=36 6x7=42 6x8=48 6x9=54 \n" +
                                "7x1=7  7x2=14 7x3=21 7x4=28 7x5=35 7x6=42 7x7=49 7x8=56 7x9=63 \n" +
                                "8x1=8  8x2=16 8x3=24 8x4=32 8x5=40 8x6=48 8x7=56 8x8=64 8x9=72 \n" +
                                "9x1=9  9x2=18 9x3=27 9x4=36 9x5=45 9x6=54 9x7=63 9x8=72 9x9=81 \n"));
    }

    @Test
    void should_get_3_to_6_multiplication_table() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiplication")
                .param("start","3").param("end", "6"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string(
                        "3x3=9  3x4=12 3x5=15 3x6=18 \n" +
                                "4x3=12 4x4=16 4x5=20 4x6=24 \n" +
                                "5x3=15 5x4=20 5x5=25 5x6=30 \n" +
                                "6x3=18 6x4=24 6x5=30 6x6=36 \n"));
    }
}