package com.twuc.webApp.controller;

import com.twuc.webApp.service.FourArithmeticTableService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tables")
public class FourArithmeticTableController {

    private FourArithmeticTableService fourArithmeticTableService;

    public FourArithmeticTableController(FourArithmeticTableService fourArithmeticTableService) {
        this.fourArithmeticTableService = fourArithmeticTableService;
    }

    @GetMapping("/plus")
    public ResponseEntity<String> getPlusTable(@RequestParam(defaultValue = "1") Integer start,
                                               @RequestParam(defaultValue = "9") Integer end) {
        String plusTable = fourArithmeticTableService.getPlusTable(start, end);

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(plusTable);
    }

    @GetMapping("/multiplication")
    public ResponseEntity<String> getMultiplicationTable(@RequestParam(defaultValue = "1") Integer start,
                                                      @RequestParam(defaultValue = "9") Integer end) {
        String multiplicationTable = fourArithmeticTableService.getMultiplicationTable(start, end);

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(multiplicationTable);
    }
}
